package arithmetic;

import CUI.Calculator;
import arithmetic.Input.Operator;
import arithmetic.Input.Digit;
import arithmetic.Input.Clear;

import java.util.LinkedList;

import static arithmetic.Input.Digit.*;
import static arithmetic.Input.Operator.*;

/**
 * Created by Jonas Fuhrmann on 11.05.16.
 *
 * This Class handles all the Input of the @CUI package.
 * The Input types are buffered in a queue and parsed to the StackHandler, which calculates the result.
 */
public class InputHandler {

    private LinkedList<InputMarker> inputBuffer;
    private NumberParser parser;
    private StackHandler stack;
    private boolean isOperator = false;
    private boolean pointSet = false;
    private boolean calculated = false;
    private Calculator mainCalculator;

    public InputHandler(Calculator calculator) {
        inputBuffer = new LinkedList<>();
        parser = new NumberParser();
        stack = new StackHandler();
        mainCalculator = calculator;
    }

    /**
     * Handles and interprets the @{@link Input}.
     * @param input is the @{@link Input} parsed from the @CUI package.
     */
    public void handle(Input input) {
        InputMarker type = input.getInputType();

        if(type instanceof Digit) {
            if(type != POINT) {
                addDigit(type);
            } else if(!pointSet) {
                pointSet = true;
                addDigit(type);
            }
        } else if(type instanceof Operator && type != EQU) {
            addOperator(type);
        } else if(type instanceof Clear) {
            switch((Clear) type) {
                case CLEAR:
                    clearAll();
                    break;
                case CLEARLAST:
                    clearLast();
                    break;
                case REVOKE:
                    revoke();
                    break;
            }
        } else if(type == EQU) {
            getCalculation(type);
        } else {
            throw new IllegalArgumentException(
                    String.format(
                            "Unsupported input type parsed.\nAllowed types: %s, %s, %s\nFound type %s",
                            Operator.class, Digit.class, Clear.class, type.getClass()
                    )
            );
        }
    }

    /**
     * Adds a Digit to the display and shows the calculation screen.
     * @param type is the @{@link InputMarker} of the @{@link Input} used in @handle.
     */
    private void addDigit(InputMarker type) {
        InputMarker peeked = inputBuffer.peekLast();
        InputMarker prePeeked = null;
        if(inputBuffer.size() > 1) {
            prePeeked = inputBuffer.get(inputBuffer.size() - 2);
        }

        if((peeked == ZERO && !(prePeeked instanceof Digit) && type != POINT) || isOperator) {
            mainCalculator.setDigits(type.toString());
            if(mainCalculator.getDisplay().equals(ZERO.toString()) || calculated) {
                mainCalculator.setDisplay(type.toString());
            } else {
                if(isOperator && type == ZERO) {
                    inputBuffer.addLast(type);
                }
                mainCalculator.addDisplayText(type.toString());
            }
            if(type != ZERO) {
                if(peeked == ZERO) inputBuffer.pollLast();
                inputBuffer.addLast(type);
            }
        } else {
            mainCalculator.addDigit(type.toString());
            mainCalculator.addDisplayText(type.toString());
            inputBuffer.addLast(type);
        }

        isOperator = false;
        calculated = false;
    }

    /**
     * Adds an Operator to the display and shows the calculation screen.
     * @param type is the @{@link InputMarker} of the @{@link Input} used in @handle.
     */
    private void addOperator(InputMarker type) {
        if(isOperator) {
            Operator peeked = (Operator) inputBuffer.peekLast();

            if((((Operator) type).isCanon()
                    && peeked != null && peeked.isCanon())
                    || type == peeked) {
                inputBuffer.pollLast();
                mainCalculator.removeLastChars(Operator.STRING_LENGTH);
            }
        }

        inputBuffer.addLast(type);
        mainCalculator.addDisplayText(((Operator) type).readableString());
        isOperator = true;
        pointSet = false;
        calculated = false;
    }

    /**
     * Uses the @{@link StackHandler} to calculate the result of the inputted @{@link Input}.
     * @param type is the @{@link InputMarker} of the @{@link Input} used in @handle.
     */
    private void getCalculation(InputMarker type) {
        if(isOperator) {
            switch((Operator) inputBuffer.peekLast()) {
                case SIGN:
                case INV:
                case CLOSEDBRACK:
                    break;
                default:
                    revoke();
            }
        }
        inputBuffer.addLast(type);
        for (InputMarker actInput : inputBuffer) {
            if (actInput instanceof Digit) {
                parser.digitInput((Digit) actInput);
            } else if (actInput instanceof Operator) {
                if (parser.getResult() != null) {
                    stack.addNumber(parser.getResult());
                    parser.reset();
                }
                if(actInput != EQU) {
                    stack.operation((Operator) actInput);
                }
            }
        }
        if(inputBuffer.size() > 0) {
            String display = Double.toString(stack.getResult());
            if(display.matches("-?[0-9]+\\.0")) {
                display = display.substring(0, display.length() - 2);
            }
            mainCalculator.setDigits(display);
            mainCalculator.setDisplay(display);
        } else {
            mainCalculator.setDigits(ZERO.toString());
            mainCalculator.setDisplay(ZERO.toString());
        }
        inputBuffer.clear();
        isOperator = true;
        calculated = true;
    }

    /**
     * Clears the calculation screen and the buffered @{@link Input}.
     */
    private void clearAll() {
        pointSet = false;
        inputBuffer.clear();
        stack.clear();
        parser.reset();
        mainCalculator.setDigits("");
        mainCalculator.setDisplay("");
        isOperator = false;
        calculated = false;
        handle(new Input(ZERO));
    }

    /**
     * Clears the last digit sequence from the calculation screen and the buffered @{@link Input}.
     */
    private void clearLast() {
        pointSet = false;
        InputMarker lastInput = inputBuffer.pollLast();
        int count = 0;
        while(lastInput instanceof Digit) {
            lastInput = inputBuffer.pollLast();
            count++;
        }
        mainCalculator.removeLastChars(count);
        if(lastInput != null) inputBuffer.addLast(lastInput);
        isOperator = true;
        calculated = false;
    }

    /**
     * Clears the last symbol from the calculation screen and the buffered @{@link Input}.
     */
    private void revoke() {
        InputMarker polled = inputBuffer.pollLast();
        if(!isOperator) {
            mainCalculator.removeLastDigit();
            mainCalculator.removeLastChars(1);
        } else if(!calculated) {
            mainCalculator.removeLastChars(STRING_LENGTH);
        }
        InputMarker peeked = inputBuffer.peekLast();
        if(peeked instanceof Operator) {
            isOperator = true;
            if(polled instanceof Digit && mainCalculator.getDisplay().isEmpty()) {
                handle(new Input(ZERO));
            }
        } else if(peeked instanceof Digit) {
            isOperator = false;
            String digits = "";
            for(int i = inputBuffer.size() - 2; peeked instanceof Digit && i >= 0; i--) {
                digits += peeked.toString();
                peeked = inputBuffer.get(i);
            }
            if(peeked instanceof Digit) {
                digits += peeked.toString();
            }
            mainCalculator.setDigits(new StringBuilder(digits).reverse().toString());
        } else if(peeked == null) {
            clearAll();
        }
        pointSet = mainCalculator.getDigits().contains(POINT.toString());
        calculated = false;
    }
}
