package arithmetic;

import static arithmetic.Input.Digit.*;
import arithmetic.Input.Digit;

import java.util.ArrayList;
import java.util.List;

final class NumberParser {
	private Double nbr; 			//Holds result and provisional result
	private boolean pointSet;		//If the parser is in decimal places mode
	private double decPlace; 		//Decimal Digit factor
    private List<Digit> numberList; //To remove a digit all digits need to be known

	NumberParser() {
		nbr = null;
		pointSet = false;
		decPlace = 1;
		numberList = new ArrayList<>();
	}

	/**
	 * Takes a digit and adds it to the number
	 * @param input new digit place added to number
     */
	void digitInput(Digit input){
		if(nbr == null){ //Initial Digit
			nbr = 0.0;
		}
		numberList.add(input); //Add number
		if(input == POINT){		//Handle points (decimal Digits)
			if(pointSet){		//two points in Number
				throw new IllegalArgumentException(
						String.format(
								"Parsed wrong argument.\nExpected %s other than %s.",
								Digit.class, Digit.POINT.name())
				);
			} 
			pointSet = true; //Rmember that a point was set
			return;
		}
		
		if(!pointSet){ //Normal Digit
			nbr *= 10;
			nbr += input.ordinal();
		} else {		//Decimal Digit
			decPlace = decPlace/10.0;
			nbr += input.ordinal() * decPlace;
		}
	}

	/**
	 * Return the Result of the parsing process
	 * @return result of Parsing
     */
	Double getResult(){
		return nbr;
	}

	/**
	 * Reset the number Parser for next number
	 */
	void reset(){
		nbr = null;
        numberList.clear();
        decPlace = 1;
        pointSet = false;
	}

	/**
	 * Remove the last Digit from input
	 */
    void removeDigit() {
        nbr = 0.0;			//reset everything
        decPlace = 1;
        pointSet = false;

        int listIndex = numberList.size()-1;
        numberList.remove(listIndex);		//Remove last digit
        for(int i = 0; i <= listIndex-1; i++){  //Parse hole number again
            digitInput(numberList.get(0));
            numberList.remove(0);
        }
    }
	
}
