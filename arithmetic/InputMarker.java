package arithmetic;

/**
 * Created by Jonas Fuhrmann on 11.05.16.
 *
 * This marker Interface provides a type for the @{@link Input} to be used by the @CUI package.
 */
interface InputMarker {
}
