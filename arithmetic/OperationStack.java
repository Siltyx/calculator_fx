package arithmetic;
import static arithmetic.Input.Operator.*;
import static arithmetic.Input.Digit.*;

import java.util.Stack;

import arithmetic.Input.Operator;

final class OperationStack {

	private Stack<Operator> opStack; //Stack for operations
	
	OperationStack(){
		opStack = new Stack<>();
	}

	/**
	 * Takes an Operator, if his priority is higher then the one on the Stack adds it, return false if not
	 * @param op Operator that should be added
	 * @return if adding the Operator was succesfull
     */
	boolean addOperator(Operator op){
		
		if(opStack.isEmpty()){	//When the stack is empty always add Operator
			opStack.push(op);
			return true;
		}

		if(opStack.peek() == OPENBRACK){	//Handling of Brackets
			if(op == CLOSEDBRACK) {			//When Closedbrack meets openbrack
				opStack.pop();				//Eliminate Brackets
				return true;
			}
			opStack.push(op);				//Every operator can be pushed over Openbrackets
			return true;
		}
		
		if(opStack.peek().getPriority() < op.getPriority()) { //Check prioritys
			opStack.push(op);
			return true;
		} else {
			return false;										//priority of new operator to low
		}
		
		
	}

	/**
	 * Number of Operators on the stack
	 * @return size of operation stack
     */
	int size() {
		return opStack.size();
	}

	/**
	 * Get the highest operator on operation Stack
	 * @return Operator on Stack
     */
	Operator popOperator(){
		if(opStack.size()>0) {		//if there is something on the Stack
			return opStack.pop();
		}
		return null;				//If there is nothing on the Stack
	}

	/**
	 * Reset the operation Stack
	 */
	void clear(){
		opStack.clear();
	}
}
