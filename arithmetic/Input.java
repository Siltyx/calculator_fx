package arithmetic;

/**
 * Created by Jonas Fuhrmann on 11.05.16.
 *
 * This Class represents the Input with the help of a marker Interface @{@link InputMarker}.
 * The @{@link InputMarker} can be one of the following enumeration classes: @{@link Operator}, @{@link Digit} or @{@link Clear}.
 */
public final class Input {

    private InputMarker inputType;
    private String symbol;

    public Input(InputMarker type) {
        inputType = type;
        symbol = type.toString();
    }

    /**
     * @return The @{@link InputMarker} of the object.
     */
    InputMarker getInputType() {
        return inputType;
    }

    @Override
    public String toString() {
        return symbol;
    }

    @Override
    public boolean equals(Object obj) {
        return inputType.equals(obj);
    }

    public enum Operator implements InputMarker {
        PLUS(1),
        MINUS(1),
        MUL(2),
        DIV(2),
        INV(3),
        SIGN(4),
        EQU(5),
        OPENBRACK(10), 		//TO DO change prioritys of brackets
        CLOSEDBRACK(-1);

        private final int priority;
        public static final int STRING_LENGTH = PLUS.readableString().length();

        Operator(int typePriority){
            priority = typePriority;
        }

        public int getPriority(){
            return priority;
        }

        public boolean isCanon() {
            switch (this) {
                case OPENBRACK:
                case CLOSEDBRACK:
                case INV:
                case SIGN:
                    return false;
                default:
                    return true;
            }
        }

        @Override
        public String toString() {
            switch(this) {
                case PLUS: return "+";
                case MINUS: return "-";
                case MUL: return "*";
                case DIV: return "/";
                case INV: return "1/x";
                case SIGN: return "\u00B1";
                case EQU: return "=";
                case OPENBRACK: return "(";
                case CLOSEDBRACK: return ")";
                default: return "?";
            }
        }

        /**
         *
         * @return A readable interpretation of the symbol.
         */
        public String readableString() {
            switch(this) {
                case PLUS: return " | + | ";
                case MINUS: return " | - | ";
                case MUL: return " | * | ";
                case DIV: return " | / | ";
                case INV: return " |1/x| ";
                case SIGN: return " | \u00B1 | ";
                case EQU: return " | = | ";
                case OPENBRACK: return " | ( | ";
                case CLOSEDBRACK: return " | ) | ";
                default: return " | ? | ";
            }
        }
    }

    public enum Digit implements InputMarker {
        ZERO, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, POINT;

        @Override
        public String toString() {
            return this.equals(Digit.POINT) ? "." : Integer.toString(this.ordinal());
        }
    }

    public enum Clear implements InputMarker {
        CLEAR, CLEARLAST, REVOKE;

        @Override
        public String toString() {
            switch (this) {
                case CLEAR: return "C";
                case CLEARLAST: return "CE";
                case REVOKE: return "BS";
                default: return "?";
            }
        }
    }
}
