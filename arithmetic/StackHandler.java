package arithmetic;
import arithmetic.Input.Operator;

import java.util.Stack;

import static arithmetic.Input.Operator.INV;
import static arithmetic.Input.Operator.SIGN;

final class StackHandler {

    private OperationStack opStack;
    private Stack<Double> nbrStack;

    StackHandler() {
        opStack = new OperationStack();
        nbrStack = new Stack<>();
    }

    /**
     * Does the operation on the two operators based on Operator given
     * @param a first operand
     * @param b second operand
     * @param op Operator used
     * @return result of operation
     */
    private double calc(double a, double b, Operator op){
        switch(op){ //Operation to the corresponding operator
            case PLUS:
                return a+b;
            case MINUS:
                return b-a;
            case MUL:
                return a*b;
            case DIV:
                return b/a;
        }
        return 0.0;
    }

    /**
     * Does an operation with just one operand
     * @param a operand
     * @param op single number operator
     * @return Result of operation
     */
    private double calc(double a, Operator op){
        if(op == INV){      //For Reciprocal
            return 1/a;
        } else if(op == SIGN){  //For sign change
            return -1*a;
        }
        return 0;
    }

    /**
     * Add operator to the operation Stack, does operations until operator can be added
     * @param op Operation that should be performed
     */
    public void operation(Operator op){
        boolean opWritten = false;              //operator added?
        double result = 0.0;                    //result of calc that will be added to Stack later
            opWritten = opStack.addOperator(op);    //Try adding operator
            while (!opWritten) {                    //if adding was unsuccessfull,  calculate result of the operator currently on top of the Stakc
                Operator tmp = opStack.popOperator();     //get op on top of the Stack
                if(tmp == SIGN || tmp == INV){          //Ckec if single number operation or multiple, then calc and save resu
                    result = calc(nbrStack.pop(), tmp);
                    nbrStack.push(result);
                    opWritten = opStack.addOperator(op);
                } else {
                    result = calc(nbrStack.pop(), nbrStack.pop(), tmp);
                    nbrStack.push(result);
                    opWritten = opStack.addOperator(op);
                }
            }

    }

    /**
     * Adds a number to the Number Stack
     * @param nbr Number thats added to the number Stack
     */
    public void addNumber(double nbr) {
        nbrStack.push(nbr);
    }

    /**
     * Calc the result of the Numbers and Operators currently on the Stack
     * @return result of calculation
     */
    public double getResult() {
        double result = 0.0;
            while (opStack.size()>0) {                  //Do as long as there are Operators on the Stack
                Operator tmp = opStack.popOperator();
                if (tmp == SIGN || tmp == INV) {        //single operand Operation or multiple
                    result = calc(nbrStack.pop(), tmp);
                    nbrStack.push(result);
                } else {
                    if(nbrStack.size() < 2){
                        return nbrStack.peek();
                    }
                    result = calc(nbrStack.pop(), nbrStack.pop(), tmp);
                    nbrStack.push(result);
                }
            }

        return nbrStack.peek(); //After calculation the top number on the Stack is the final result
    }

    /**
     * Clear StackHandler
     */
    void clear(){
        opStack.clear();
        nbrStack.clear();
    }
}
