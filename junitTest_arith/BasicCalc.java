package junitTest_arith;


import arithmetic.StackHandler;
import arithmetic.StackHandler;
import org.junit.Before;
import org.junit.Test;

import static arithmetic.Input.Operator.*;
import static junit.framework.TestCase.assertEquals;

/**
 * Created by S on 12.05.2016.
 */
public class BasicCalc {
    
    @Test
    public void basicTest() {
        StackHandler test = new StackHandler();
        test.addNumber(5);
        test.operation(PLUS);
        test.addNumber(3);
        test.operation(DIV);
        test.addNumber(3);
        assertEquals(6, test.getResult(), 0);
    }


    @Test
    public void basicCombinedTest() {
        StackHandler test = new StackHandler();
        test.addNumber(8);
        test.operation(PLUS);
        test.addNumber(4);
        test.operation(MUL);
        test.addNumber(4);
        test.operation(DIV);
        test.addNumber(8);
        test.operation(MINUS);
        test.addNumber(7);

        assertEquals(3, test.getResult(), 0);
    }

    @Test
    public void basicTestDecimalDigits() {
        StackHandler test = new StackHandler();
        test.addNumber(15.5);
        test.operation(MUL);
        test.addNumber(2);
        test.operation(MINUS);
        test.addNumber(7);
        test.operation(PLUS);
        test.addNumber(28.32);
        test.operation(DIV);
        test.addNumber(5.41);

        assertEquals(15.5*2-7+28.32/5.41, test.getResult(), 0);
    }

    @Test
    public void basicBrackTest() {
        StackHandler test = new StackHandler();
        test.addNumber(15.5);
        test.operation(MUL);
        test.operation(OPENBRACK);
        test.addNumber(2);
        test.operation(MINUS);
        test.addNumber(7);
        test.operation(CLOSEDBRACK);
        test.operation(PLUS);
        test.addNumber(28.32);
        test.operation(DIV);
        test.addNumber(5.41);

        assertEquals(15.5*(2-7)+28.32/5.41, test.getResult(), 0);
    }

    @Test
    public void basicBrackTest2() {
        StackHandler test = new StackHandler();
        test.addNumber(15.5);
        test.operation(MUL);
        test.operation(OPENBRACK);
        test.operation(OPENBRACK);
        test.addNumber(2);
        test.operation(MINUS);
        test.addNumber(7);
        test.operation(CLOSEDBRACK);
        test.operation(PLUS);
        test.addNumber(28.32);
        test.operation(CLOSEDBRACK);
        test.operation(DIV);
        test.addNumber(5.41);

        assertEquals(15.5*((2-7)+28.32)/5.41, test.getResult(), 0);
    }

    @Test
    public void pmTest() {
        StackHandler test = new StackHandler();
        test.addNumber(15.5);
        test.operation(MUL);
        test.operation(OPENBRACK);
        test.operation(OPENBRACK);
        test.addNumber(2);
        test.operation(MINUS);
        test.addNumber(7);
        test.operation(CLOSEDBRACK);
        test.operation(PLUS);
        test.addNumber(28.32);
        test.operation(CLOSEDBRACK);
        test.operation(DIV);
        test.addNumber(5.41);
        test.operation(SIGN);

        assertEquals(15.5*((2-7)+28.32)/-5.41, test.getResult(), 0);
    }

    @Test
    public void invTest() {
        StackHandler test = new StackHandler();
        test.addNumber(15.5);
        test.operation(MUL);
        test.operation(OPENBRACK);
        test.operation(OPENBRACK);
        test.addNumber(2);
        test.operation(MINUS);
        test.addNumber(7);
        test.operation(CLOSEDBRACK);
        test.operation(PLUS);
        test.addNumber(28.32);
        test.operation(CLOSEDBRACK);
        test.operation(DIV);
        test.addNumber(5.41);
        test.operation(INV);

        assertEquals(15.5*((2-7)+28.32)/(1/5.41), test.getResult(), 0);
    }

    @Test
    public void minus(){
        StackHandler test = new StackHandler();
        test.addNumber(2);
        test.operation(MINUS);
        test.addNumber(7);
        assertEquals(-5, test.getResult(), 0);
    }

    @Test
    public void plus(){
        StackHandler test = new StackHandler();
        test.addNumber(2);
        test.operation(PLUS);
        test.addNumber(7);
        assertEquals(9, test.getResult(), 0);
    }

    @Test
    public void mul(){
        StackHandler test = new StackHandler();
        test.addNumber(2);
        test.operation(MUL);
        test.addNumber(7);
        assertEquals(14, test.getResult(), 0);
    }

    @Test
    public void div(){
        StackHandler test = new StackHandler();
        test.addNumber(2);
        test.operation(DIV);
        test.addNumber(4);
        assertEquals(0.5, test.getResult(), 0);
    }
}