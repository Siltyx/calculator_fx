package junitTest_arith;


import arithmetic.NumberParser;
import org.junit.Before;
import org.junit.Test;

import static arithmetic.Input.Digit.*;
import static org.junit.Assert.assertEquals;

public class NumberParserTest {

	@Test
	public void basicTest() {
		NumberParser parser = new NumberParser();
		parser.digitInput(FIVE);
		assertEquals(5.0,parser.getResu(), 0);
	}
	
	@Test
	public void decimalPlaceTestOne() {
		NumberParser parser = new NumberParser();
		parser.digitInput(FIVE);
		parser.digitInput(POINT);
		parser.digitInput(FOUR);
		assertEquals(5.4,parser.getResu(), 0);
	}
	
	@Test
	public void decimalPlaceTestTwo() {
		NumberParser parser = new NumberParser();
		parser.digitInput(FIVE);
		parser.digitInput(POINT);
		parser.digitInput(FOUR);
		parser.digitInput(THREE);
		parser.digitInput(TWO);
		parser.digitInput(ONE);
		assertEquals(5.4321,parser.getResu(), 0);
	}

	@Test
	public void digitClearTest(){
		NumberParser parser = new NumberParser();
		parser.digitInput(FIVE);
		parser.digitInput(POINT);
		parser.digitInput(FOUR);
		parser.digitInput(THREE);
		parser.digitInput(TWO);
		parser.digitInput(ONE);
		parser.removeDigit();
		assertEquals(5.432,parser.getResu(), 0);
	}

	@Test
	public void digitClearTest2(){
		NumberParser parser = new NumberParser();
		parser.digitInput(FIVE);
		parser.digitInput(POINT);
		parser.digitInput(FOUR);
		parser.digitInput(THREE);
		parser.digitInput(TWO);
		parser.digitInput(ONE);
		parser.removeDigit();
		parser.removeDigit();
		assertEquals(5.43,parser.getResu(), 0);
	}

	@Test
	public void digitClearTest3(){
		NumberParser parser = new NumberParser();
		parser.digitInput(FIVE);
		parser.digitInput(POINT);
		parser.digitInput(FOUR);
		parser.digitInput(THREE);
		assertEquals(5.43,parser.getResu(), 0);
	}



}
