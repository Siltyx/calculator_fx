package CUI;

import arithmetic.Input;
import arithmetic.InputHandler;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Created by Jonas Fuhrmann on 09.05.16.
 *
 * The {@link Calculator} Class acts as the main application.
 * Use @main to start the UI or implement it manually by calling the @launch method.
 */

public class Calculator extends Application {

    private CalculatorPane calculatorPane;

    /**
     * Starts the user interface.
     *
     * @param primaryStage is the main stage provided by @launch or @main method.
     */

    @Override
    public void start(Stage primaryStage) throws IllegalArgumentException {
        InputHandler handler = new InputHandler(this);
        calculatorPane = new CalculatorPane(handler);
        Scene scene = new Scene(calculatorPane, 250, 300);
        handler.handle(new Input(Input.Digit.ZERO));

        calculatorPane.requestFocus();

        primaryStage.setTitle("Calculator");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    /**
     * Displays the given @text on the internal @{@link DisplayField}.
     *
     * @param text to be displayed.
     */

    public void setDigits(String text) {
        calculatorPane.setDigits(text);
    }

    public void setDisplay(String text) {
        calculatorPane.setDisplay(text);
    }

    public void addDigit(String text) {
        String tmp = getDigits();
        tmp += text;
        setDigits(tmp);
    }

    public void removeLastDigit() {
        setDigits(removeLastChars(getDigits(), 1));
    }

    public void addDisplayText(String text) {
        String tmp = getDisplay();
        tmp += text;
        setDisplay(tmp);
    }

    public void removeLastChars(int subStringSize) {
        setDisplay(removeLastChars(getDisplay(), subStringSize));
    }

    private String removeLastChars(String text, int size) {
        return text.substring(0, text.length() - size);
    }

    public String getDigits() {
        return calculatorPane.getDigits();
    }

    public String getDisplay() {
        return  calculatorPane.getDisplay();
    }

    /**
     * The main function of the Javafx application.
     *
     * @param args are the parsed arguments.
     */

    public static void main(String[] args) {
        launch(args);
    }
}
