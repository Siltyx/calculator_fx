package CUI;

import arithmetic.InputHandler;
import javafx.concurrent.Task;
import javafx.geometry.Pos;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

/**
 * Created by Jonas Fuhrmann on 11.05.16.
 *
 * The @{@link CalculatorPane} represents the displayed content of the @{@link Calculator}.
 * It contains a @{@link DisplayField}, shown as the display, and a @{@link KeyPadPane},
 * containing the @{@link CalcButton}s.
 */

class CalculatorPane extends GridPane {

    private DisplayField calcDisplay;
    private DisplayField digitDisplay;
    private KeyPadPane keyPad;

    CalculatorPane(InputHandler inputHandler) throws IllegalArgumentException {
        super();
        calcDisplay = new DisplayField();
        digitDisplay = new DisplayField();
        keyPad = new KeyPadPane(inputHandler);
        addRow(1, calcDisplay);
        addRow(2, digitDisplay);
        addRow(3, keyPad);

        setMaxWidth(Double.MAX_VALUE);
        setMaxHeight(Double.MAX_VALUE);
        setHgrow(keyPad, Priority.ALWAYS);
        setVgrow(keyPad, Priority.ALWAYS);
    }

    /**
     * Displays the given @text on the internal @{@link DisplayField}.
     *
     * @param text to be displayed.
     */

    void setDigits(String text) {
        digitDisplay.setText(text);
    }

    void setDisplay(String text) {
        calcDisplay.setText(text);
        calcDisplay.requestFocus();
        calcDisplay.positionCaret(text.length());
    }

    String getDigits() {
        return digitDisplay.getText();
    }

    String getDisplay() {
        return calcDisplay.getText();
    }
}
