/**
 * This package represents the visual part of a calculator application.
 * It requires the latest version of the arithmetic package to be functional.
 *
 * The @{@link CUI.Calculator} class acts as the main application and can be used to start the user interface.
 *
 * @author Jonas Fuhrmann, Sebastian Brückner
 * @version 1.0
 *
 */
package CUI;