package CUI;

import arithmetic.Input;
import arithmetic.InputHandler;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;

import java.util.ArrayList;

import static arithmetic.Input.Digit.*;
import static arithmetic.Input.Operator.*;
import static arithmetic.Input.Clear.*;

/**
 * Created by Jonas Fuhrmann on 09.05.16.
 *
 * The @{@link KeyPadPane} represents the button field of the @{@link Calculator}.
 * It holds all @{@link CalcButton}s shown on the UI.
 */

class KeyPadPane extends GridPane {

    private InputHandler buttonHandler;

    // digits
    private Button numbers[];

    // basic operators
    private Button equalButton;
    private Button plusButton;
    private Button minusButton;
    private Button mulButton;
    private Button divButton;
    private Button signButton;

    // advanced operators
    private Button inverseButton;

    // remaining buttons
    private Button clearButton;
    private Button clLineButton;
    private Button revokeButton;

    private Button commaButton;
    private Button brackOpenButton;
    private Button brackCloseButton;


    KeyPadPane(InputHandler inputHandler) throws IllegalArgumentException {
        super();
        buttonHandler = inputHandler;
        initButtons();

        setMaxWidth(Double.MAX_VALUE);
        setMaxHeight(Double.MAX_VALUE);
        for(Node child : getChildren()) {
            setHgrow(child, Priority.ALWAYS);
            setVgrow(child, Priority.ALWAYS);
        }
    }

    /**
     * Initializes all @{@link CalcButton}s.
     */

    private void initButtons() throws IllegalArgumentException {
        initNumbers();
        initBasics();
        initRemaining();

        addNumbers();
        addBasics();
        addRemaining();
    }

    /**
     * Initializes the digit @{@link CalcButton}s.
     * Uses @{@link arithmetic.Input.Digit} enumeration.
     */

    private void initNumbers() throws IllegalArgumentException {
        numbers = new Button[]{
                new CalcButton(new Input(ZERO), buttonHandler),
                new CalcButton(new Input(ONE),  buttonHandler),
                new CalcButton(new Input(TWO),  buttonHandler),
                new CalcButton(new Input(THREE),buttonHandler),
                new CalcButton(new Input(FOUR), buttonHandler),
                new CalcButton(new Input(FIVE), buttonHandler),
                new CalcButton(new Input(SIX),  buttonHandler),
                new CalcButton(new Input(SEVEN),buttonHandler),
                new CalcButton(new Input(EIGHT),buttonHandler),
                new CalcButton(new Input(NINE), buttonHandler)
        };
    }

    /**
     * Initializes the basic operators @{@link CalcButton}s.
     * Uses @{@link arithmetic.Input.Operator} enumeration.
     */

    private void initBasics() throws IllegalArgumentException {
        equalButton =   new CalcButton(new Input(EQU),  buttonHandler);
        plusButton =    new CalcButton(new Input(PLUS), buttonHandler);
        minusButton =   new CalcButton(new Input(MINUS),buttonHandler);
        mulButton =     new CalcButton(new Input(MUL),  buttonHandler);
        divButton =     new CalcButton(new Input(DIV),  buttonHandler);
        signButton =    new CalcButton(new Input(SIGN), buttonHandler);
    }

    /**
     * Initializes the remaining @{@link CalcButton}s.
     * Uses @{@link arithmetic.Input.Clear}, @{@link arithmetic.Input.Operator}
     * and @ {@link arithmetic.Input.Digit} enumerations.
     */

    private void initRemaining() throws IllegalArgumentException {
        inverseButton = new CalcButton(new Input(INV), buttonHandler);

        clearButton  =  new CalcButton(new Input(CLEAR),    buttonHandler);
        clLineButton =  new CalcButton(new Input(CLEARLAST),buttonHandler);
        revokeButton =  new CalcButton(new Input(REVOKE),   buttonHandler);

        commaButton      =  new CalcButton(new Input(POINT),        buttonHandler);
        brackOpenButton  =  new CalcButton(new Input(OPENBRACK),    buttonHandler);
        brackCloseButton =  new CalcButton(new Input(CLOSEDBRACK),  buttonHandler);
    }

    /**
     * Adds the digit @{@link CalcButton}s to the @{@link KeyPadPane}.
     */

    private void addNumbers() {
        add(numbers[0], 2, 4);
        add(numbers[1], 1, 3);
        add(numbers[2], 2, 3);
        add(numbers[3], 3, 3);
        add(numbers[4], 1, 2);
        add(numbers[5], 2, 2);
        add(numbers[6], 3, 2);
        add(numbers[7], 1, 1);
        add(numbers[8], 2, 1);
        add(numbers[9], 3, 1);
    }

    /**
     * Adds the basic @{@link CalcButton}s to the @{@link KeyPadPane}.
     */

    private void addBasics() {
        add(signButton,     1, 4);
        add(plusButton,     4, 2);
        add(minusButton,    5, 2);
        add(mulButton,      4, 3);
        add(divButton,      5, 3);
        add(equalButton,    4, 4);
    }

    /**
     * Adds the remaining @{@link CalcButton}s to the @{@link KeyPadPane}.
     */

    private void addRemaining() {
        add(commaButton,        3, 4);
        add(brackOpenButton,    4, 1);
        add(brackCloseButton,   5, 1);
        add(inverseButton,      5, 4);
        add(revokeButton,       6, 1);
        add(clLineButton,       6, 2);
        add(clearButton,        6, 3);
    }
}
