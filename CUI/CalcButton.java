package CUI;

import arithmetic.Input;
import arithmetic.InputHandler;
import javafx.scene.control.Button;
import javafx.scene.text.Font;

/**
 * Created by Jonas Fuhrmann on 10.05.16.
 *
 * The @{@link CalcButton} represents a button on the @{@link Calculator}, added to the @{@link KeyPadPane}.
 * It calls the @{@link arithmetic.InputHandler} @handle method to refresh the input data.
 */

class CalcButton extends Button {
    CalcButton(Input input, InputHandler inputHandler) throws IllegalArgumentException {
        super(input.toString());

        setFont(Font.font("Roboto" ,15));
        setStyle(
                "-fx-focus-color: transparent;" +
                "-fx-faint-focus-color: transparent;"
        );

        setMaxWidth(Double.MAX_VALUE);
        setMaxHeight(Double.MAX_VALUE);

        setOnAction(event -> inputHandler.handle(input));
    }
}
