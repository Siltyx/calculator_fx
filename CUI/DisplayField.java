package CUI;

import javafx.geometry.Pos;
import javafx.scene.control.TextField;
import javafx.scene.text.Font;

/**
 * Created by Jonas Fuhrmann on 11.05.16.
 *
 * The @{@link DisplayField} represents the display of the @{@link Calculator}.
 */
class DisplayField extends TextField {
     DisplayField() {
         super();
         setEditable(false);
         setAlignment(Pos.BASELINE_RIGHT);
         setFont(Font.font("Roboto" ,20));
         setStyle(
                 "-fx-focus-color: transparent;" +
                 "-fx-faint-focus-color: transparent;" +
                 "-fx-background-color: -fx-control-inner-background;" +
                 "-fx-background-insets: 0;" +
                 "-fx-padding: 1 3 1 3;"
         );
     }
}
